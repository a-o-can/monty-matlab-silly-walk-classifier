function [windowedData, labels] = extractData(matFileContent, matFileName, targetSamplingRateHZ, windowLengthSeconds)    
    %% Check if time vector of measurement is unique
    %To prevent non-uniqueness
    if numel(matFileContent.time)~=numel(unique(matFileContent.time))
       %we have non unique entries: kick out one of the non unique entries
       [matFileContent.time,idx] = unique(matFileContent.time,'first');
       matFileContent.data=matFileContent.data(:, idx);
    end    
    %% Original Data
    data = matFileContent.data;
    time = matFileContent.time;

    x = data(1, :);
    y = data(2, :);
    z = data(3, :);
    matFileName = char(matFileName);
    
    %% Interpolation
    stepSize = 1/targetSamplingRateHZ;
    timeInter = 0:stepSize:time(end);    
    xInter = interp1(time, x, timeInter);
    yInter = interp1(time, y, timeInter);
    zInter = interp1(time, z, timeInter);
    dataInter = double([xInter; yInter; zInter]);

    %% Windowing
    windowLength = targetSamplingRateHZ * windowLengthSeconds;
    windowLength = floor(windowLength);
    % Check if the length is even
    if mod(windowLength, 2) ~= 0
        windowLength = windowLength - 1;       
    end
    % Number of windows to extract
    numWindows = floor(length(timeInter)/(windowLength/2)) - 1;
    % Initialize an array of zeros for the windowed data
    windowedArray  = zeros(3, windowLength, numWindows);
    for i = 0:numWindows-1
        start = i*(windowLength/2) + 1;
        finish = start + windowLength - 1;
        windowedArray(:, :, i+1) = dataInter(:, start:finish);
    end
    % Convert to a cell array of shape (numWindows x 1)
    windowedData = reshape(num2cell(windowedArray, [1, 2]), numWindows, 1);

    %% Labels
    label = matFileName(end-4);
    if label == 'N'
        label = 'Normal walk';
    else
        label = 'Silly walk';
    end
    labels = cell(numWindows, 1);
    labels(:) = {label};
    labels = categorical(labels);
end