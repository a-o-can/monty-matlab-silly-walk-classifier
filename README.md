# Monty Matlab 2022 Group 1 
Khalil Smaoui, Malek Dhiab, Ali Oğuz Can, Fatma Dirman

## Requirements
Matlab Toolboxes:

- Deep Learning Toolbox

- Signal Processing Toolbox

- Statistics and Machine Learning Toolbox

## How to run
Run the *pipeline.m* script to start the classification pipeline for our main model (LSTM Network).
In order to run classification pipeline for 1D-CNN and kNN, change folder either to 1D-CNN/ or kNN/ and run the
corresponding *pipeline* script from there.

## Launch GUI 
Run the *main.m* script to start the GUI. 
