function [XFeatures] = extractFeatureKNN(XTrain)
%{
extractFeatureKNN:
    The function extractFeature extract the predifined features in features_for_KNN for a given input data
    containing time sequence data. It returns the extracted features by calling the function features_for_KNN.
     Input:
    XTrain:     %cell array containing n double matrices of size 3xM (3
                 chanels).
    
     Output:
    XFeatures:  %double array of size nxf with n samples and f features(6 features for this model)
%}    

    for i=1:length(XTrain)
         [features.mag_mean(i,1),features.sum_mag_25(i,1),features.sum_mag_75(i,1), ...
          features.max_freq(i,1),features.freq_comp_5Hz(i,1),features.nm_peaks_Hz(i,1)]...
          = features_for_KNN(XTrain{i});
    end
    XFeatures = [features.mag_mean, features.sum_mag_25, features.sum_mag_75, ...
                  features.max_freq, features.freq_comp_5Hz, features.nm_peaks_Hz];
end