% Make sure that you are inside kNN folder

trainPath = "TrainingData";
testPath = "TestData";



% Add necessary paths
parentDirectory = fileparts(cd);
addpath(parentDirectory);
utilsPath = fullfile(parentDirectory ,"utils");
addpath(utilsPath)

trainPath = fullfile(parentDirectory, "TrainingData");
testPath = fullfile(parentDirectory, "TestData");

SamplingRate = 50;
WindowLength = 3.4;

[XTrain, XTest, YTrain, YTest] = createData(trainPath, testPath, SamplingRate, WindowLength);

XTrainFeatures = extractFeatureKNN(XTrain);
XTestFeatures = extractFeatureKNN(XTest);
YTrain = categorical(YTrain);

knn_model = trainKNN(XTrainFeatures, YTrain, 4, 'euclidian');
YPred = predict(knn_model, XTestFeatures);
acc = mean(YPred == YTest);
disp("Test Accuracy: " + num2str(acc));


confusionchart(YTest,YPred)

% Add necessary paths
rmpath(utilsPath);
rmpath(parentDirectory);