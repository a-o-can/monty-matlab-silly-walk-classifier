function [mag_mean, sum_mag_25, sum_mag_75, max_freq, freq_comp_5Hz, nm_peaks_Hz] = features_for_KNN(X)
%{
The function features_for_KNN extract the given features for a given
Matrix.
It returns the extracted features in the given variables. 
Input:
 X:             Double matrice that contains time sequence data. 
Output:
 mag_mean:      Mean value of acceleration magnitude.
 sum_mag_25:    Sum of magnitude under 25 percentile
 sum_mag_75:    Sum of magnitude under 75 percentile
 max_freq       Maximum frequency in spectrum
 freq_comp_5Hz  Sum of frequency components below 5Hz
 nm_peaks_Hz    Number of peaks in spectrum above 5Hz
%}


mag_mean = mean(vecnorm(X,2,1));

sum_mag_25 = prctile(vecnorm(X,2,1),25);
sum_mag_75 = prctile(vecnorm(X,2,1),75);

%FFT parameters for spectrum
L =length(X(1,:));
Fs = 100;
full_fr=linspace(0,Fs,L);
spectrum  = fft(vecnorm(X,2,1))/L;
[~,freq] = findpeaks(abs(spectrum),full_fr);

max_freq = max(freq);
freq_comp_5Hz = sum(freq<5);
nm_peaks_Hz = length(freq(freq>5));
end