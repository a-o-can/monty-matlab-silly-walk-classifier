function [XTrain, XTest, YTrain, YTest] = createData(trainPath, testPath, targetSamplingRateHZ, windowLengthSeconds)

%{
    This function, given a path for training and test .mat files, returns the
    cell array of training samples, cell array of test samples, categorical
    array of train labels, categorical array of test labels.

    Parameter:
        trainPath [string]: Path to training data folder.
        testPath [string]: Path to test data folder.

    Return:
        XTrain [Nx1 cell array]: Consists of training samples. Each sample
                                 is a 3x170 double matrix.
        XTest [Nx1 cell array]:  Consists of test samples. Each sample is a 
                                 3x170 double matrix.
        YTrain [Nx1 categorical array]: Consists of class labels, 
                                        'Normal walk' or 'Silly walk', for
                                        training samples.
        YTest [Nx1 categorical array]: Consists of class labels, 
                                       'Normal walk' or 'Silly walk', for
                                       test samples.
%}
    addpath("utils");
    
    %% Train Data
    trainFilenames = get_file_names(trainPath, "mat");
    XTrain = {};
    YTrain = categorical([]);
    for i=1:length(trainFilenames)
        fileName = trainFilenames(i);
        content = load(fullfile(trainPath, fileName{1}));
        [X, y] = extractData(content, fileName{1}, targetSamplingRateHZ, windowLengthSeconds);
        XTrain(end+1:end+length(X)) = X;
        YTrain(end+1:end+length(y)) = y;
    end
    XTrain = reshape(XTrain, size(XTrain, 2), size(XTrain, 1));
    YTrain = reshape(YTrain, size(YTrain, 2), 1);

    %% Test Data
    testFilenames = get_file_names(testPath, "mat");
    XTest = {};
    YTest = categorical([]);
    for i=1:length(testFilenames)
        fileName = testFilenames(i);
        content = load(fullfile(testPath, fileName{1}));
        [X, y] = extractData(content, fileName{1}, targetSamplingRateHZ, windowLengthSeconds);
        XTest(end+1:end+length(X)) = X;
        YTest(end+1:end+length(y)) = y;
    end
    XTest = reshape(XTest, size(XTest, 2), size(XTest, 1));
    YTest = reshape(YTest, size(YTest, 2), 1);

    %% Remove added path
    rmpath("utils");
end