% Make sure that you are inside LSTM folder


% Add necessary paths
parentDirectory = fileparts(cd);
addpath(parentDirectory);
utilsPath = fullfile(parentDirectory ,"utils");
addpath(utilsPath);

trainPath = fullfile(parentDirectory, "TrainingData");
testPath = fullfile(parentDirectory, "TestData");


pwd
SamplingRate = 50;
WindowLength = 3.4;

[XTrain, XTest, YTrain, YTest] = createData(trainPath, testPath, SamplingRate, WindowLength);

model = trainLSTM(XTrain, YTrain);
YPred = classifyWalk(model, XTest);

% Evaluate model

acc = mean(YPred == YTest);
disp("Test Accuracy: " + num2str(acc));


confusionchart(YTest,YPred)

% Remove paths
rmpath(parentDirectory);
rmpath(utilsPath);