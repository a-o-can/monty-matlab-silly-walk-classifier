function model = trainSillyWalkClassifier(XTrain, YTrain)

    
    % Randomly select indices to split data into 90% training, 10%
    % validation, 0% test set.
    
    [train_idx, val_idx, ~] = dividerand(size(XTrain,1), 0.9, 0.1, 0);
    
    X = XTrain; Y = YTrain;
    XTrain = X(train_idx, :);
    XVal = X(val_idx, :);
    YTrain = Y(train_idx);
    YVal = Y(val_idx);
    
    % Set network parameters
    numFeatures = size(XTrain{1},1); % 3 Features: X, Y, Z
    numHiddenUnits = 30;
    classes = categories(YTrain);
    numClasses = numel(classes);
    maxEpochs = 60;
    miniBatchSize = 32;    
    model_arch = [ ...
        sequenceInputLayer(numFeatures)
        lstmLayer(numHiddenUnits,'OutputMode','sequence')
        dropoutLayer(0.5)
        lstmLayer(numHiddenUnits,'OutputMode','last')
        dropoutLayer(0.5)
        reluLayer
        fullyConnectedLayer(numClasses)
        softmaxLayer
        classificationLayer];

options = trainingOptions('adam', ...
    'MaxEpochs',maxEpochs, ...
    'MiniBatchSize',miniBatchSize, ...
    'GradientThreshold',1, ...
    'Plots','none', ...
    'ValidationData',{XVal, YVal}, ...
    'Verbose',0);
    
    model = trainNetwork(XTrain, YTrain, model_arch, options);
    
    save(fullfile(fileparts(mfilename('fullpath')), 'Model_LSTM.mat'), 'model'); % do not change this line
    
end