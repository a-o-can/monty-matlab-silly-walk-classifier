function resample_data(path_to_data, sampling_frequency, path_to_save_dir)
    [~, fName, fExt] = fileparts(path_to_data);
        
    matFileContent = load(path_to_data);
    data = matFileContent.data;
    time = matFileContent.time;
    
    x = data(1,:);
    y = data(2,:);
    z = data(3,:);
    
    % Interpolate
    stepSize = 1/sampling_frequency;
    time_interpolated = 0:stepSize:time(end); 
    x_interpolated = interp1(time, x, time_interpolated);
    y_interpolated = interp1(time, y, time_interpolated);
    z_interpolated = interp1(time, z, time_interpolated);
    
    clear data;
    clear time;
    time = time_interpolated;
    data = [x_interpolated; y_interpolated; z_interpolated];
    
    save_string = fullfile(path_to_save_dir, fName + fExt);
    save(save_string, 'data', 'time');
end