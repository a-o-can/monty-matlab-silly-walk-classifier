function delete_contents(folder_path, file_type)
%{
    This function, given a folder path and a specific type of data, deletes
    the respective files

    Parameter:
        folder_path [string]: The path to the folder
        file_type [string]: The extention of files you are looking for
        e.g. "mat"
    
    Return:
        None
    Example Usage:
        delete_contents(fullfile("Users", "Max", "documents", "data"), "mat");
%}
    delete(fullfile(folder_path , "*." + file_type))
end