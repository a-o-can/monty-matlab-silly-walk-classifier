function file_names = get_file_names(folder_path, file_type)
%{
    This function, given a folder path and a specific type of data, returns
    the names of the files

    Parameter:
        folder_path [string]: The path to the folder
        file_type [string]: The extention of files you are looking for
        e.g. "mat"
    
    Return:
        file_names [1xN cell]: A cell of char arrays containing the names
        of the files
    Example Usage:
        file_names = get_file_names(fullfile("Users", "Max", ...
                                      "documents", "data"), "mat");
%}
    files = dir(fullfile(folder_path, "*." + file_type));
    files_struct = struct2cell(files);
    file_names = files_struct(1,:);
end