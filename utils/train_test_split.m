function train_test_split(test_set_ratio, path_data_dir, path_project_root)
%{
    This function creates a balanced partitioning of test/train set.
    
    Assumption:
        i) The data folders are exactly named as follows: DATA_Normal and
        DATA_Silly
        ii) The TestData/TrainingData folders are located in the root
        directory of the project.
    
    Parameter:
        test_set_ratio [float]: Test to train set ration, e.g. 0.2
        path_data_dir [string]: Path to the folder where DATA_Normal and
            DATA_Silly are located, i.e. path of 'utils' folder.
        path_project_root [string]: Path to the project root, i.e
            fullfile("whereever_your_project_repo_is", g1_2022)
    
    Return:
        None
    
    Example Usage:
        path_data_dir = fullfile("Users", "Max", "Projects", "g1_2022", "utils")
        path_project_root = fullfile("Users", "Max", "Projects", "g1_2022")
        train_test_split(0.2, path_data_dir, path_project_root)
%}
    %% Define paths according to the conventions
    data_normal_path = fullfile(path_data_dir, "DATA_Normal");
    data_silly_path = fullfile(path_data_dir, "DATA_Silly");
    test_set_path = fullfile(path_project_root, "TestData");
    train_set_path = fullfile(path_project_root, "TrainingData");
    
    %% Define data set sizes and respective indices
    normal_data_size = get_folder_size(data_normal_path, "mat");
    silly_data_size = get_folder_size(data_silly_path, "mat");
    total_data_size = normal_data_size + silly_data_size;
    
    test_set_size = floor(total_data_size * test_set_ratio);
    % Ensure that the test set size is actually an even number
    if mod(test_set_size, 2) ~= 0
        test_set_size = test_set_size - 1;
    end
    
    % Define two different indices lists to ensure class balance to avoid
    % overfitting
    randomized_indices_S = randperm(silly_data_size);
    randomized_indices_N = randperm(normal_data_size);
    
    test_set_indices_S = randomized_indices_S(1:(test_set_size/2));
    train_set_indices_S = randomized_indices_S((test_set_size/2)+1 : end);
    
    test_set_indices_N = randomized_indices_N(1:(test_set_size/2));
    train_set_indices_N = randomized_indices_S((test_set_size/2)+1 : end);
    
    %% Read in names of files
    file_names_N = get_file_names(data_normal_path, "mat");
    file_names_S = get_file_names(data_silly_path, "mat");
    
    %% Save data to respective test/train set folders
    % Make sure the partitioning from a previous time is deleted
    delete_contents(test_set_path, "mat");
    for i = test_set_indices_S
       data_to_be_saved = file_names_S(i);
       source = fullfile(data_silly_path, data_to_be_saved);
       destination = fullfile(test_set_path, data_to_be_saved);
       copyfile(source, destination);
    end
    for i = test_set_indices_N
       data_to_be_saved = file_names_N(i);
       source = fullfile(data_normal_path, data_to_be_saved);
       destination = fullfile(test_set_path, data_to_be_saved);
       copyfile(source, destination);
    end
    
    delete_contents(train_set_path, "mat");
    for i = train_set_indices_S
       data_to_be_saved = file_names_S(i);
       source = fullfile(data_silly_path, data_to_be_saved);
       destination = fullfile(train_set_path, data_to_be_saved);
       copyfile(source, destination);
    end
    for i = train_set_indices_N
       data_to_be_saved = file_names_N(i);
       source = fullfile(data_normal_path, data_to_be_saved);
       destination = fullfile(train_set_path, data_to_be_saved);
       copyfile(source, destination);
    end
    
end