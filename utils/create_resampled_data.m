%% Oguz Normal
currentFolder = pwd();
fileName = "oguzNormal";
path_to_data_dir = fullfile(currentFolder, fileName);
files = dir(fullfile(currentFolder, fileName, '*.mat'));
saveFileName = "oguzNormal_Resampled";
path_to_save_dir = fullfile(currentFolder, saveFileName);

filesCell = struct2cell(files);
filesCell = filesCell(1,:);

numFiles = size(filesCell,2);

for i = 1:numFiles
   file = filesCell{i};
   path_to_data = fullfile(path_to_data_dir, file);
   resample_data(path_to_data, 60, path_to_save_dir);
end

%% Oguz Silly
fileName = "oguzSilly";
saveFileName = "oguzSilly_Resampled";
path_to_data_dir = fullfile(currentFolder, fileName);
files = dir(fullfile(currentFolder, fileName, '*.mat'));
path_to_save_dir = fullfile(currentFolder, saveFileName);

filesCell = struct2cell(files);
filesCell = filesCell(1,:);
numFiles = size(filesCell,2);

for i = 1:numFiles
   file = filesCell{i};
   path_to_data = fullfile(path_to_data_dir, file);
   resample_data(path_to_data, 60, path_to_save_dir);
end

%% Subject1 Normal
fileName = "subject1Normal";
saveFileName = "subject1Normal_Resampled";
path_to_data_dir = fullfile(currentFolder, fileName);
files = dir(fullfile(currentFolder, fileName, '*.mat'));
path_to_save_dir = fullfile(currentFolder, saveFileName);

filesCell = struct2cell(files);
filesCell = filesCell(1,:);
numFiles = size(filesCell,2);

for i = 1:numFiles
   file = filesCell{i};
   path_to_data = fullfile(path_to_data_dir, file);
   resample_data(path_to_data, 60, path_to_save_dir);
end

%% Subject1 Silly
fileName = "subject1Silly";
saveFileName = "subject1Silly_Resampled";
path_to_data_dir = fullfile(currentFolder, fileName);
files = dir(fullfile(currentFolder, fileName, '*.mat'));
path_to_save_dir = fullfile(currentFolder, saveFileName);

filesCell = struct2cell(files);
filesCell = filesCell(1,:);
numFiles = size(filesCell,2);

for i = 1:numFiles
   file = filesCell{i};
   path_to_data = fullfile(path_to_data_dir, file);
   resample_data(path_to_data, 60, path_to_save_dir);
end
