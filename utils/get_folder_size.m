function folder_size = get_folder_size(folder_path, file_type)
%{
    This function, given a folder path and a specific type of data, returns
    you the number of files within the folder.

    Parameter:
        folder_path [string]: The path to the folder
        file_type [string]: The extention of files you are looking for
        e.g. "mat"
    
    Return:
        folder_size [int]: Number of files in the given folder

    Example Usage:
        folder_size = get_folder_size(fullfile("Users", "Max", ...
                                      "documents", "data"), "mat");
%}
    tmp = fullfile(folder_path, "*." + file_type);
    tmp=dir(tmp);
    folder_size = size(tmp,1);
end