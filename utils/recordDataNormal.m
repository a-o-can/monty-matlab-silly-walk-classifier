clear all;

name = 'write_your_own_name';
folderName = sprintf('%sNormal', name);
[~, ~, ~] = mkdir(folderName);
files = dir(fullfile(folderName, '*.mat'));
filesLen = size(files, 1);
fileName = fullfile(folderName, sprintf('Group1_Walk%i_N.mat', filesLen+1));

m = mobiledev;
m.AccelerationSensorEnabled = 1;
m.SampleRate = 60;

disp('Starts counting down from 5s')
delay(5);
disp('5s elapsed: Data sending starts!')
% Start sending data
m.Logging = 1;

delay(15)
disp('15s elapsed: Data sending stops!')
% Stop sending data
m.Logging = 0;
saveToMat(fileName, m);

function saveToMat(f, m)
    % Retrieve XYZ acceleration
    [dataAcc, timeAcc] = accellog(m);
    data = dataAcc';
    time = timeAcc';

    % Save the variables to a .mat file
    save(f, 'data', 'time')

    % Plot
    plot(timeAcc, dataAcc);
    legend('X', 'Y', 'Z');
    xlabel('Relative time (s)');
    ylabel('Acceleration (m/s^2)');

end

function delay(seconds)
    tic;
    while toc < seconds
    end
end
    