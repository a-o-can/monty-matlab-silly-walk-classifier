function model = trainSillyWalkClassifier(XTrain, YTrain)

    % For this trivial example, no model is required. 
    numFeatures = size(XTrain{1},1); % 3 Features: X, Y, Z
    classes = categories(YTrain);
    numClasses = numel(classes);
    disp("Detected number of classes: " + int2str(numClasses));
    
    % Randomly select indices to split data into 90% training, 10%
    % validation, 0% test set.
    [train_idx, val_idx, ~] = dividerand(size(XTrain,1), 0.9, 0.1, 0);
    
    X = XTrain; Y = YTrain;
    XTrain = X(train_idx, :);
    XVal = X(val_idx, :);
    YTrain = Y(train_idx);
    YVal = Y(val_idx);
    
    filterSize = 3;
    numFilters = 32;
    
    model_arch = [ ...
        sequenceInputLayer(numFeatures)
        convolution1dLayer(filterSize,numFilters,Padding="causal")
        reluLayer
        layerNormalizationLayer
        convolution1dLayer(filterSize,2*numFilters,Padding="causal")
        reluLayer
        layerNormalizationLayer
        globalAveragePooling1dLayer
        fullyConnectedLayer(numClasses)
        softmaxLayer
        classificationLayer];
    
    miniBatchSize = 27;
    
    options = trainingOptions("adam", ...
        MiniBatchSize=miniBatchSize, ...
        MaxEpochs=15, ...
        SequencePaddingDirection="left", ...
        ValidationData={XVal, YVal}, ...
        Plots="none", ...
        Verbose=0);
    
    model = trainNetwork(XTrain, YTrain, model_arch, options);    
end