% Make sure you are inside kNN folder

% Add necessary paths
parentDirectory = fileparts(cd);
addpath(parentDirectory);
utilsPath = fullfile(parentDirectory ,"utils");
addpath(utilsPath);

trainPath = fullfile(parentDirectory, "TrainingData");
testPath = fullfile(parentDirectory, "TestData");

SamplingRate = 50;
WinowLength = 3.4;

% Button
% 1) Start Training Button
[XTrain, XTest, YTrain, YTest] = createData(trainPath, testPath, SamplingRate, WinowLength);
model = train_CNN(XTrain, YTrain);


% Button: Make Predictions
YPred = classifyWalk(model, XTest);


% Evaluate model
acc = mean(YPred == YTest);

disp("Test Accuracy: " + num2str(acc));

% 1 Button for each metric
confusionchart(YTest,YPred)

% Remove paths
rmpath(parentDirectory);
rmpath(utilsPath);
