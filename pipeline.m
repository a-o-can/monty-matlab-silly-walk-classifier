trainPath = "TrainingData";
testPath = "TestData";

SamplingRate = 50;
WinowLength = 3.4;

% Button
% 1) Start Training Button
[XTrain, XTest, YTrain, YTest] = createData(trainPath, testPath, SamplingRate, WinowLength);
model = trainSillyWalkClassifier(XTrain, YTrain);


% Button: Make Predictions
YPred = classifyWalk(model, XTest);


% Evaluate model
acc = mean(YPred == YTest);

disp("Test Accuracy: " + num2str(acc));

% 1 Button for each metric
confusionchart(YTest,YPred)