% Check required toolboxes if are installed
required_toolboxes = {'Deep Learning Toolbox',...
                      'Signal Processing Toolbox',... 
                      'Statistics and Machine Learning Toolbox'};
installed_toolboxes = ver;
ls = {};
for i = installed_toolboxes
    ls{end+1} = i;
end

for x = required_toolboxes
    if any(strcmp(ls,x))
        % User does not have the toolbox installed.
        text = ['Sorry, but you do not seem to have the'  char(x) '.\nDo you want to try to continue anyway?'];
	    message = sprintf(text);
	    reply = questdlg(message, 'Toolbox missing', 'Yes', 'No', 'Yes');
	    if strcmpi(reply, 'No')
		    % User said No, so exit.
		    return;
	    end
    end

end

% Add necessary paths
addpath("GUI", "LSTM", "CNN", "kNN", "utils");
% Start GUI
GUI();
